# References

*  https://spring.io/projects/spring-boot
*  https://dzone.com/
*  https://github.com/OAI/OpenAPI-Specification
*  https://swagger.io/tools/swagger-editor/
*  https://prometheus.io/
*  https://grafana.com/
*  https://aws.amazon.com/fr/
*  https://dzone.com/articles/spring-boot-best-practices-for-microservices
*  https://getbootstrap.com/
*  https://jquery.com/
*  https://angularjs.org/
*  https://reactjs.org/
*  https://www.archimatetool.com/
*  https://www.opengroup.org/togaf
