package org.astrakhan.demo.model;

public class Product {
	
	private String id;
	private String description;
	private String name;
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Product() {
		// TODO Auto-generated constructor stub
	}
	
	public Product(String name, String id, String desc ) {
		this.setDescription(desc);
		this.setId(id);
		this.setName(name);
		

	}
	

}
