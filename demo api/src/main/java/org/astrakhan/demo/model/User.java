package org.astrakhan.demo.model;

public class User {
	
	private String first_name;
	private String last_name;
	private String email;
	private int userId;
	


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getBirth_town() {
		return birth_town;
	}


	public void setBirth_town(String birth_town) {
		this.birth_town = birth_town;
	}


	private String birth_town;
	

	public User() {
		// TODO Auto-generated constructor stub
	}


	public User(String first_name, String last_name, String email, String birth_town) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.birth_town = birth_town;
	}
	
	

}
