package org.astrakhan.demo.web.controller;

import java.util.List;

import org.astrakhan.demo.dao.UserDao;
import org.astrakhan.demo.model.Channel;
import org.astrakhan.demo.model.Tweet;
import org.astrakhan.demo.model.User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {
	
	@GetMapping(value="/users")
    public List<User> Users() {
		UserDao udao = new UserDao();	
        return udao.getAllUsers();
    }
	
	@PostMapping("/users")
	public User newUser(@RequestBody User newUser) {
		
	    return null;
	 }
	
	@DeleteMapping("/users/{userId}")
	void deleteUser(@PathVariable(value = "User Id") int userId) {
		
	}
	
	
	@GetMapping(value = "/users/{userId}/channels")
	public List<Channel> listChannels(@PathVariable int userId) {
		
		return null;
	}

	@GetMapping(value = "/users/{userId}/channels/{id}")
	public List<Tweet> showChannelById(@PathVariable int userId, @PathVariable int id) {
		
		return null;
	}
	
	@PostMapping("/users/{userId}/channels")
	public Channel newChannel(@PathVariable int userId, @RequestBody Channel newChannel) {
		
	    return null;
	}
	
	@DeleteMapping("/users/{userId}/channels/{id}")
	void deleteChannel(@PathVariable int userId, @PathVariable int id) {
		
	}
	
	@PostMapping("/users/{userId}/channels/{id}/tweet")
	public Tweet newTweet(@PathVariable int userId, @PathVariable int id, @RequestBody Tweet newTweet) {
		
	    return null;
	}
	
	@DeleteMapping("/users/{userId}/channels/{id}/tweet/{tweetId}")
	void deleteTweet(@PathVariable int userId, @PathVariable int id, @PathVariable int tweetId) {
		
	}


}
