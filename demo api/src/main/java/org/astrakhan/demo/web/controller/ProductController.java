package org.astrakhan.demo.web.controller;

import java.util.List;

import org.astrakhan.demo.dao.ProductDao;
import org.astrakhan.demo.model.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public List<Product> listeProduits() {
		ProductDao pdao = new ProductDao();
		return pdao.findAll();

	}

	@GetMapping(value = "/products/{id}")
	public Product afficherUnProduit(@PathVariable String id) {
		ProductDao pdao = new ProductDao();
		return pdao.findById(id);

	}

}
