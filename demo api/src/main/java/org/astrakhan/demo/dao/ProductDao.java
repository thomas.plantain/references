package org.astrakhan.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.astrakhan.demo.model.Product;

public class ProductDao {
	
	
	public static List<Product> products = new ArrayList<Product>();
    static {
        products.add(new Product("laptop", "001", "iBook"));
        products.add(new Product("laptop", "002", "iBook Air")); 
        products.add(new Product("laptop", "003", "Dell"));
    }
    
    private static Product livre = new Product("Livre", "004", "Eyrolles");

   
    public List<Product> findAll() {
        return products;
    }
    
    public Product findById(String id) {
    	
        return livre;
    }
    

	public ProductDao() {
		// TODO Auto-generated constructor stub
	}

}
