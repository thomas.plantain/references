package org.astrakhan.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.astrakhan.demo.model.User;

public class UserDao {
	
	public static List<User> users = new ArrayList<User>();
	
    static {
    	users.add(new User("Thomas", "Plantain", "thomas.plantain@astrakhan.fr", "Montmorency"));
    	users.add(new User("Bernard", "Plantain", "bernard.plantain@astrakhan.fr", "Paris")); 
    	users.add(new User("John", "McLeod", "john.leod@astrakhan.fr", "New-York"));
    }
	
	
	public List<User> getAllUsers() {
		
		return users;
		
	}

	public UserDao() {
		// TODO Auto-generated constructor stub
	}

}
