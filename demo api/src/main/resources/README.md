#  Micro service Démo 

### Documentation OpenAPI 3
- [SwaggerUI](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)
- [Swagger OA3](http://localhost:8080/v3/api-docs)

### Services
- GET [http://localhost:8080/Users](http://localhost:8080/Users)
- GET [http://localhost:8080/Products](http://localhost:8080/Products)
- GET [http://localhost:8080/Products/{id}](http://localhost:8080/Products/001)